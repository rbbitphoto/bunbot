const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('user')
		.setDescription('Replies with User Info'),
	async execute(interaction) {
		await interaction.reply(
			`The Degenerate's Name: ${interaction.user.tag}
			\nThe Degenerate: ${interaction.user.displayAvatarURL({ dynamic: true, size: 512})}`
		);
	},
};