const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('server')
		.setDescription('Replies with Server Info'),
	async execute(interaction) {
		await interaction.reply(
            `Server info: ${interaction.guild.name}
            \nTotal Members: ${interaction.guild.memberCount}`
        );
	},
};