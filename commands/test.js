const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('test')
		.setDescription('Replies with Test Successful!'),
	async execute(interaction) {
		await interaction.reply('Test Successful!');
	},
};