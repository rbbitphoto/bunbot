const fs = require('node:fs');
const path = require('node:path');
const { token } = require('./config.json');
const { Client, Collection, Intents } = require('discord.js');
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
// const messagesPath = path.join(__dirname, 'messages');
// const messagesfiles = fs.readdirSync(messagesPath).filter(file => file.endsWith('.js'));

const client = new Client({ intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES
]});

client.commands = new Collection();

for (const file of eventFiles) {
	const filePath = path.join(eventsPath, file);
	const event = require(filePath);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}

for (const file of commandFiles) {
	const filePath = path.join(commandsPath, file);
	const command = require(filePath);
	client.commands.set(command.data.name, command);
}

client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;
    const command = client.commands.get(interaction.commandName);
	if (!command) return;
	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply(
            { content: 'There was an error while executing this command!', 
            ephemeral: true 
        });
	}
});

const Jaoobisms = [
    "So true!",
    "What can I say? Get fucked.",
    "We're gaming!",
    "Feels bad, chief.",
    "That was extremely unfortunate.",
    "Do you want to see something funny?",
    "... League?",
    "Pretty cringe, not gonna lie.",
    "HELL YEAH, BROTHER",
    "See ya later, nerds!",
    "... No",
    "JaOoB",
    "<:jaoobSEEN:981697267713060904>",
    "No matter.",
    "Fuck you, Greg."
];

let jaoobTrigger = false;
client.on('messageCreate', (message) => {
    if (jaoobTrigger) {
        jaoobTrigger = false;
        return;
    }
    if (message.content.toLowerCase().includes('!jaoob')) {
        jaoob = Jaoobisms[
            Math.floor(
                Math.random() * Jaoobisms.length
            )
        ];
        if (!jaoobTrigger) {
        message.reply(`Jaoob says: ${jaoob}`)
        jaoobTrigger = true
        console.log(`Sent Jaoobism: ${jaoob}`)
        }
        return;
    }
})

const Lyndaisms = [
    "<:lizardmoment:977005236524814377> ",
    "Hello!",
    "Have you looked at the docs?",
    "Umm. I'm not quite able to answer that question.",
    "Could this wait till after the lecture?",
    "I would use Google.",
    "Remember, Google is your friend!",
    "You will not be moving onto Module 2.",
    "I see.",
    "I really want you to grok with this stuff."
];

let lyndaTrigger = false;
client.on('messageCreate', (message) => {
    if (lyndaTrigger) {
        lyndaTrigger = false;
        return;
    }
    if (message.content.toLowerCase().includes('!lynda')) {
        lynda = Lyndaisms[
            Math.floor(
                Math.random() * Lyndaisms.length
            )
        ];
        if (!lyndaTrigger) {
        message.reply(`Lynda says: ${lynda}`)
        lyndaTrigger = true
        console.log(`Sent Lyndaism: '${lynda}'`)
        }
        return;
    }
})

const Paulisms = [
    "Seeing all of you grow has been incredible.",
    "You're a rockstar!",
    "Y'all are wild! LMFAO",
    "The tears lube your code!",
    ":)",
    "You know so much more than you think.",
    "Remember me when you're all rich!",
    "I look forward to seeing you have a wonderful career!",
    "It's okay if you don't understand this now, you're learning."
];

let paulTrigger = false;
client.on('messageCreate', (message) => {
    if (paulTrigger) {
        paulTrigger = false;
        return;
    }
    if (message.content.toLowerCase().includes('!paul')) {
        paul = Paulisms[
            Math.floor(
                Math.random() * Paulisms.length
            )
        ];
        if (!paulTrigger) {
        message.reply(`Paul says: ${paul}`)
        paulTrigger = true
        console.log(`Sent Paulism: '${paul}'`)
        }
        return;
    }
})

client.login(token);